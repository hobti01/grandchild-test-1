#!/usr/bin/env bash

set -e

if hash envsubst 2>/dev/null; then
  envsubst "$(printf '$%s ' $(compgen -v -X '[a-z]*'))" <"$1" >"$2"
else
  cp "$1" "$2"
fi
